package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
)

func main() {
	cmd := exec.Command("go", "mod", "init", "test")
	if len(os.Args) > 1 {
		cmd.Dir = os.Args[2]
	}
	cmd.Run()

	cmd = exec.Command("go", "test", "-cover")
	if len(os.Args) > 1 {
		cmd.Dir = os.Args[2]
	}

	// Выполнить команду и вернуть вывод
	output, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatalf("Failed to execute command: %s", cmd)
	}

	fmt.Println(string(output))
}
