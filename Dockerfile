# Этап сборки
FROM golang:1.19.9-alpine AS builder

# Устанавливаем рабочий каталог
WORKDIR /app

# Копируем файлы модуля и скачиваем зависимости
COPY go.* ./
RUN go mod download

# Копируем исходные коды
COPY . .

# Компилируем бинарный файл
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o taskcheck ./cmd/taskcheck

# Этап запуска
FROM golang:1.19.9-alpine

# Копируем бинарный файл из этапа сборки
COPY --from=builder /app/taskcheck /usr/local/bin/taskcheck

# Задаем точку входа
ENTRYPOINT ["/usr/local/bin/taskcheck"]
