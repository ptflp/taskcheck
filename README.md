# Taskcheck

# Build
```bash
docker build -t ptflp/taskcheck .
```

# Run
```bash
docker run -v /some/path:/some/path ptflp/taskcheck taskcheck /some/path
```
